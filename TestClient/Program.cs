﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using s3i_cs_lib;
using System;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace TestClient
{
    [DataContract]
    public class DirThingHarvester {
        [DataMember]
        public string identifier { get; set; }
        [DataMember]
        public string policyId { get; set; }
        [DataMember]
        public DirHarvesterAttributes attributes { get; set; }
}

    [DataContract]
    public class DirHarvesterAttributes
    {
        [DataMember]
        public string ownedBy { get; set; }
        [DataMember]
        public Position position { get; set; }
        [DataMember]
        public List<string> allEndpoints { get; set; }
    }

    [DataContract]
    public class Position
    {
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public string latitude { get; set; }
}

    class Program
    {
        public static void Main()
        {
            /*
             *
             * You can use this block to choose the test-function
             * 
             * All other blocks should be commented out
             * 
             */

            //DirectoryTests().GetAwaiter().GetResult();

            GetTokenTest().GetAwaiter().GetResult();

            // Decrypted Messages
            //SendDecryptedUMsg().GetAwaiter().GetResult();
            //ReceiveDecryptedUMsg().GetAwaiter().GetResult();

            
            Demo().GetAwaiter().GetResult();

        }

        private static async Task Demo()
        {

            Console.WriteLine(" ");
            Console.WriteLine("Let´s login");
            string xy = Console.ReadLine();

            // get Token
            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);


            Console.WriteLine(" ");
            Console.WriteLine("Let´s create a thing");
            xy = Console.ReadLine();

            // create thing
            Config conf = new Config("https://config.s3i.vswf.dev/", token);
            string responseCT = await conf.CreateThing(false);

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(CreateThingResponse));
            byte[] byteArray = Encoding.UTF8.GetBytes(responseCT);
            MemoryStream stream2 = new MemoryStream(byteArray);
            CreateThingResponse res = (CreateThingResponse)ser.ReadObject(stream2);
            Console.WriteLine("new thing identifier: " + res.identifier);
            string newThingID = res.identifier;

            Console.WriteLine(" ");
            Console.WriteLine("Let´s query our new thing");
            xy = Console.ReadLine();

            // query thingID based
            DittoManager dir = new S3IDirectory("https://dir.s3i.vswf.dev/api/2/", token);
            string response = await dir.QueryThingIDBasedAsync(newThingID);

            DataContractJsonSerializer serharvdir = new DataContractJsonSerializer(typeof(DirThingHarvester));
            byteArray = Encoding.UTF8.GetBytes(response);
            stream2 = new MemoryStream(byteArray);
            DirThingHarvester harvesterDir = (DirThingHarvester)serharvdir.ReadObject(stream2);
            Console.WriteLine("new thing is owned by: " + harvesterDir.attributes.ownedBy);
            Console.WriteLine("complete data stack: " + response);

            Console.WriteLine(" ");
            Console.WriteLine("Let´s update our thing");
            xy = Console.ReadLine();

            // update position
            harvesterDir.attributes.position = new Position();
            harvesterDir.attributes.position.longitude = "006.034489";
            harvesterDir.attributes.position.latitude = "50.784241";

            MemoryStream stream3 = new MemoryStream();
            serharvdir.WriteObject(stream3, harvesterDir);
            string payload = Encoding.ASCII.GetString(stream3.ToArray());
            response = await dir.UpdateThingIDBasedAsync(newThingID, payload);


            Console.WriteLine(" ");
            Console.WriteLine("Let´s query our thing again");
            xy = Console.ReadLine();

            // query thingID based
            response = await dir.QueryThingIDBasedAsync(newThingID);

            byteArray = Encoding.UTF8.GetBytes(response);
            MemoryStream stream4 = new MemoryStream(byteArray);
            harvesterDir = (DirThingHarvester)serharvdir.ReadObject(stream4);
            Console.WriteLine("new thing is at position: " + harvesterDir.attributes.position.latitude + ", " + harvesterDir.attributes.position.longitude);
            Console.WriteLine("complete data stack: " + response);

            Console.WriteLine(" ");

            Console.WriteLine(await dir.DeleteAttribute(newThingID, "position"));

            response = await dir.QueryThingIDBasedAsync(newThingID);

            byteArray = Encoding.UTF8.GetBytes(response);
            MemoryStream stream5 = new MemoryStream(byteArray);
            harvesterDir = (DirThingHarvester)serharvdir.ReadObject(stream5);
            Console.WriteLine("complete data stack: " + response);

            Console.WriteLine(await conf.DeleteThing(newThingID));
        }

        private static async Task DirectoryTests()
        {
            // (Test) Get Token

            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);
            //Console.WriteLine("token:" + token);

            /*
            // Test query thingID based
            DittoManager dm = new DittoManager("https://ditto.s3i.vswf.dev/api/2/", token);
            string response = await dm.QueryThingIDBasedAsync("s3i:0708c68c-0a8a-42a9-887b-93724f6d5926");
            Console.WriteLine("response:" + response);
            */

            /*
            // Test query attribute based
            DittoManager dm = new DittoManager("https://ditto.s3i.vswf.dev/api/2/", token);
            string responseAttr = await dm.QueryAttributeBasedAsync("name", "Test");
            Console.WriteLine("response:" + responseAttr);
            */

            /*
            // Test search all without parameter
            DittoManager dm = new DittoManager("https://ditto.s3i.vswf.dev/api/2/", token);
            string responseAttr = await dm.SearchAllAsync();
            Console.WriteLine("response:" + responseAttr);
            */

            /*
            // Test create person
            Config conf = new Config("https://config.s3i.vswf.dev/", token);
            Console.WriteLine("[S3I]: Please enter the new username: \n");
            string newUsername = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the new password: \n");
            string newPassword = Console.ReadLine();
            string responseCP = await conf.CreatePerson(newUsername, newPassword);
            Console.WriteLine("response:" + responseCP);
            */

            /*
            // Test delete person
            Config conf = new Config("https://config.s3i.vswf.dev/", token);
            string responseCP = await conf.DeletePerson("Testperson-Harry");
            Console.WriteLine("response:" + responseCP);
            */
        }

        private static async Task ReceiveDecryptedUMsg()
        {
            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);

            S3IDirectory s3iDir = new S3IDirectory("https://dir.s3i.vswf.dev/api/2/", token);
            String hmiSchmitzEndpoint = "s3ibs://s3i:6f58e045-fd30-496d-b519-a0b966f1ab01";

            List<string> receiverEndpoints = new List<string>();

            /*
            foreach (var i in hmiSchmitzEndpoints)
            {
                string si = (String)i;
                if (si.StartsWith("s3ibs://"))
                {
                    receiverEndpoints.Add(si);
                    Console.WriteLine(si);
                }
            }
            */

            s3i_cs_lib.Broker demo = new s3i_cs_lib.Broker("Username/Password", " ", token, "application/json", null, "rabbitmq.s3i.vswf.dev");
            Console.WriteLine("Start Receive");
            demo.Receive(hmiSchmitzEndpoint, true, (result) => callbackDecryptedUMsg(result));
            Console.ReadLine();
        }

        private static async Task SendDecryptedUMsg()
        {
            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);
            S3IDirectory s3iDir = new S3IDirectory("https://dir.s3i.vswf.dev/api/2/", token);
            string dtSchmitz = await s3iDir.QueryAttributeBasedAsync("thingStructure/links/target/values/value", "Schmitz");
            JObject jsonSchmitz = JObject.Parse(dtSchmitz);
            string hmiSchmitzId = (String)jsonSchmitz["items"][0]["attributes"]["defaultHMI"];
            string sender_endpoint = "s3ib://s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959";
            string sender = "s3i:2aafd97c-ff05-42b6-8e4d-e492330ec959";
            string hmiSchmitz = await s3iDir.QueryThingIDBasedAsync(hmiSchmitzId);
            JObject jsonHmiSchmitz = JObject.Parse(hmiSchmitz);
            JArray hmiSchmitzEndpoints = (JArray)jsonHmiSchmitz["attributes"]["defaultEndpoints"];

            List<string> receiverEndpoints = new List<string>();
                        
            foreach (var i in hmiSchmitzEndpoints){
                string si = (String)i;
                if (si.StartsWith("s3ibs://")) {
                    receiverEndpoints.Add(si);
                }
            }

            List<string> receivers = new List<string>();
            receivers.Add(hmiSchmitzId);
            s3i_cs_lib.Broker demoWaldarbeiter = new s3i_cs_lib.Broker("Username/Password", " ", token, "rabbitmq.s3i.vswf.dev", null, "rabbitmq.s3i.vswf.dev");

            Console.WriteLine("[S3I]: Please enter the subject: \n");
            string subject = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the text: \n");
            string text = Console.ReadLine();
            string msg_uuid = "s3i:" + Guid.NewGuid();

            UserMessage uMsg = new UserMessage();
            uMsg.FillUserMessage(sender, receivers, sender_endpoint, subject, text, msg_uuid);
            demoWaldarbeiter.Send(receiverEndpoints, Encoding.ASCII.GetBytes(uMsg.getMsg()));
        }

        private static void callbackDecryptedUMsg(string message)
        {
            Console.WriteLine("Result from callback: " + message);
            UserMessage UMessage = new UserMessage();
            UMessage.setMsg(message.Replace("'", "\""));
            Console.WriteLine(UMessage.msg.subject);
        }

        private static async Task GetTokenTest(){
        
            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("[S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);
            Console.WriteLine("token:" + token);
        
        }

    }
}