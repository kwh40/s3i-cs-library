﻿using System;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
//using System.Text.Json;
using System.Linq;
using s3i_cs_lib;
using s3i_cs_lib.Policy;
using s3i_cs_lib.Directory;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.IO;

namespace ReworkTest
{
    class Program
    {
        private static async Task GetToken()
        {
            Console.WriteLine("[S3I]: Please enter the username: \n");
            string username = Console.ReadLine();
            Console.WriteLine("\n [S3I]: Please enter the password: \n");
            string password = Console.ReadLine();
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", username, password, "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);
            Console.WriteLine(token);
        }


        public static async Task PolicyTest()
        {
            // Feste Anmeldung:
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", "Stefan", "hauler123", "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);

            //string response = await QueryPolicyIDBasedAsync("s3i:006adaef-a490-495b-b004-58bfb6eb8fab", "https://dir.s3i.vswf.dev/api/2/", token);

            PolicyEntryFactory fac = new PolicyEntryFactory();

            //PolicyEntry pol = await PolicyEntryFactory.Pull("s3i:006adaef-a490-495b-b004-58bfb6eb8fab", token, "https://dir.s3i.vswf.dev/api/2/");
            PolicyEntry pol = await fac.Pull(token, "https://dir.s3i.vswf.dev/api/2/", "s3i:006adaef-a490-495b-b004-58bfb6eb8fab");
            pol.GetGroup("observer").AddSubject("nginx:test", "test");
            Console.WriteLine("Neue Policy: {0}\n\n", pol.PolicyToString());
            string res = await fac.Commit(token, "https://dir.s3i.vswf.dev/api/2/", "s3i:006adaef-a490-495b-b004-58bfb6eb8fab", pol.PolicyToString());
            Console.WriteLine(res + "\n\n");

            pol = await fac.Pull(token, "https://dir.s3i.vswf.dev/api/2/", "s3i:006adaef-a490-495b-b004-58bfb6eb8fab");
            Console.WriteLine("Überprüfung: {0}", pol.PolicyToString());


            //Group newGr = pol.AddGroup("Test Group");

            //    dynamic json = @"{'policy:/ ': {
            //  'grant': [
            //    'READ',
            //    'WRITE''
            //  ],
            //  'revoke': []
            //},}";
            //    newGr.AddResource(JsonConvert.DeserializeObject(json));
            //    newGr.AddSubject("Test ID", "Test Type");

            //    Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            //    Console.WriteLine(pol);



            //foreach (KeyValuePair<string, Group> g in pol.GetPolicyGroups())
            //{
            //    Console.WriteLine("Gruppeninhalte: " + g.Value);
            //}
            //await PolicyEntryFactory.Commit(token, "https://dir.s3i.vswf.dev/api/2/", "s3i:006adaef-a490-495b-b004-58bfb6eb8fab", );

            //pol.AddGroup("Test");

            //foreach (KeyValuePair<string, Group> g in pol.GetPolicyGroups())
            //{
            //    Console.WriteLine("Gruppenbezeichnung: " + g.Key);
            //}

            //pol.DeleteGroup("Test");

            //foreach (KeyValuePair<string, Group> g in pol.GetPolicyGroups())
            //{
            //    Console.WriteLine("Gruppenbezeichnung: " + g.Key);
            //}

            //Subject s = pol.GetGroup("owner").GetSubject("nginx:s3i:006adaef-a490-495b-b004-58bfb6eb8fab");
            //Console.WriteLine(s.GetType());
            //s.SetType("Test Test");
            //Console.WriteLine(s.GetType());

            //pol.GetGroup("owner").AddResource("test", Resource.PermissionType.Read | Resource.PermissionType.Write, Resource.PermissionType.Read);
            //Group gr = pol.GetGroup("owner");

            //foreach (KeyValuePair<string, Resource> e in gr.GetGroupResources())
            //{
            //    Resource sub = e.Value;
            //    Console.WriteLine("Path: " + sub.GetPath() + "\tGrant: " + sub.GetGrant() + "\tRevoke: " + sub.GetRevoke());
            //}

            //Console.WriteLine(gr.DeleteResource("test"));

            //foreach (KeyValuePair<string, Resource> e in gr.GetGroupResources())
            //{
            //    Resource sub = e.Value;
            //    Console.WriteLine("Path: " + sub.GetPath() + "\tGrant: " + sub.GetGrant(), "\tRevoke: " + sub.GetRevoke());
            //}
        }

        public static async Task<string> QueryPolicyIDBasedAsync(String thingID, string dittoURL, string token)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(dittoURL + "things/" + thingID);
            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public static async Task<string> SearchAllAsync(string dittoURL, string token)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(dittoURL + "search/things/");
            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public static async Task DirectoryTest()
        {
            // Abruf Token:
            IdentityProvider idp = new IdentityProvider("password", "admin-cli", "Stefan", "hauler123", "", "KWH", "https://idp.s3i.vswf.dev/");
            string token = await idp.GetTokenAsync(3, true);

            // Abruf Eintrag nach ID:
            string response = await QueryPolicyIDBasedAsync("s3i:006adaef-a490-495b-b004-58bfb6eb8fab", "https://dir.s3i.vswf.dev/api/2/", token);

            // Abruf aller Einträge in Directory:
            //string response = await SearchAllAsync("https://dir.s3i.vswf.dev/api/2/", token);


            // Erstellung von Thing: 
            Thing thing = new Thing(response);

            // Object- und Value-Klassen Tests
            //s3i_cs_lib.Directory.Object ob = thing.GetThingStructure();
            //List<Link> l = ob.GetLinks();
            //List<Value> v = l[1].GetTarget().GetValues();

            thing.AddEndpoint("test");

            thing.RemoveEndpoint("test");
        }

        static void Main(string[] args)
        {
            //Demo();
            //PolicyTest().GetAwaiter().GetResult();
            DirectoryTest().GetAwaiter().GetResult();
            //GetToken().GetAwaiter().GetResult();

            ////IEnumerable<JToken> e = json.SelectTokens("$.attributes.thingStructure.links[?(@.target.class == 'ml40::Thing')]");
            //IEnumerable<JToken> e = json.SelectTokens("$.attributes.thingStructure.links[?(@.association == 'features')].target.links[?(@.class == 'ml40::Thing')]");
            //foreach (JToken i in e)
            //{
            //    Console.WriteLine(i + "\n");
            //}
        }
    }
}
