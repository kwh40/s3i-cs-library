﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s3i_cs_lib
{
    namespace Miscellaneous
    {
        public class Location
        {
            private double longitude;
            private double latitude;

            public Location(double longitude, double latitude)
            {
                this.longitude = longitude;
                this.latitude = latitude;
            }

            
            public double Longitude { get => longitude; set => longitude = value; }
            public double Latitude { get => latitude; set => latitude = value; }
        }
    }
}
