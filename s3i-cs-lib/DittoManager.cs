﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace s3i_cs_lib
{
    public class DittoManager
    {
        private String dittoURL;
        private String token;

        public DittoManager(String newDittoURL, String newToken)
        {
            this.dittoURL = newDittoURL;
            this.token = newToken;
        }

        public async Task<string> UpdateThingIDBasedAsync(String thingID, String payload)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PutAsync(this.dittoURL + "things/" + thingID, requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> UpdatePolicyIDBasedAsync(String policyID, String payload)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PutAsync(this.dittoURL + "policies/" + policyID, requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> QueryThingIDBasedAsync(String thingID)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(dittoURL + "things/" + thingID);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> QueryPolicyIDBasedAsync(string policyID)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.token);
            HttpResponseMessage response = await client.GetAsync(this.dittoURL + "policies/" + policyID);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> QueryAttributeBasedAsync(String key, String value)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(this.dittoURL + "search/things?filter=eq(attributes/" + key + ",\"" + value + "\")");

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> SearchAllAsync()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(dittoURL + "search/things");

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> SearchAllAsync(String queryParameter)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.GetAsync(dittoURL + "search/things?" + queryParameter);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeleteAttribute(string thingId, string attributePath)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.DeleteAsync(dittoURL + "things/" + thingId + "/attributes/" + attributePath);
            
            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeleteFeature(string thingId, string featurePath)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.DeleteAsync(dittoURL + "things/" + thingId + "/features/" + featurePath);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return "successful deleted";
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeleteFeatureProperty(string thingId, string featurePath, string propertyPath)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            HttpResponseMessage response = await client.DeleteAsync(dittoURL + "things/" + thingId + "/features/" + featurePath + "/properties/" + propertyPath);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return "successful deleted";
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }
    }
}
