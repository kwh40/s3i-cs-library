﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;


namespace s3i_cs_lib
{
    [DataContract]
    public class AuthRequestBody {
        [DataMember]
        public String grantType { get; set; }
        [DataMember]
        public String clientId { get; set; }
        [DataMember]
        public String clientSecret { get; set; }
        [DataMember]
        public String username { get; set; }
        [DataMember]
        public String password { get; set; }
        [DataMember]
        public String scope { get; set; }
    }

    public class TokenBundle {
        [DataMember]
        public String access_token { get; set; }
        [DataMember]
        public int expires_in { get; set; }
        [DataMember]
        public int refresh_expires_in { get; set; }
        [DataMember]
        public String refresh_token { get; set; }
        [DataMember]
        public String token_type { get; set; }
        [DataMember]
        public int not_before_policy { get; set; }
        [DataMember]
        public String session_state { get; set; }
        [DataMember]
        public String scope { get; set; }
    }

    public class IdentityProvider
    {

        private String grantType;
        private String clientId;
        private String username;
        private String password;
        private String clientSecret;
        private String realm;
        private String identityProviderURL;
        private String identityProviderGetToken;
        private String identityProviderHeader;
        private Boolean tokenInspectorRun;
        private DateTime lastLogin;
        private String IdentityProviderGetPubKey;
        private HttpClient client;
        private int tokenType;
        private String tokenBundleString = "";
        private String scope;
        public String responseString;
        private TokenBundle tokenBundle;

        public IdentityProvider(String grantType, String clientId, String username, String password, String clientSecret, String realm, String identityProviderURL) {
            this.grantType = grantType;
            this.clientId = clientId;
            this.username = username;
            this.password = password;
            this.clientSecret = clientSecret;
            this.realm = realm;
            this.identityProviderURL = identityProviderURL;
            identityProviderGetToken = this.identityProviderURL + "auth/realms/" + this.realm + "/protocol/openid-connect/token";
            client = new HttpClient();
            //client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            tokenInspectorRun = false;
            //lastLogin = 0;
            IdentityProviderGetPubKey = this.identityProviderURL + "auth/realms/" + this.realm + "/protocol/openid-connect/certs";
        }

        public void StopRunForever() {
            tokenInspectorRun = false;
        }

        public void RunForever(int tokenType, String onNewToken, int sleepInterval) {
            if (tokenType != 4) {

                this.tokenInspectorRun = true;
            }
        }

        public async Task<string> GetTokenAsync(int tokenType, Boolean requestNew) {
            if (tokenInspectorRun) {
                return null;
            }
            if (requestNew)
            {
                await Authenticate();
            }
            else {
                if (tokenBundle == null) {
                    await Authenticate();
                }
                if (TimeUntilTokenValid() <= 0) {
                    await Authenticate();
                }
            }
            if (tokenType == 3) {
                //Console.WriteLine(string.Concat("Access Token: ", tokenBundle.access_token)); 
                return tokenBundle.access_token;
            }   
/*            else if (tokenType == 2){
                //Console.WriteLine(string.Concat("Access Token: ", tokenBundle.id_token));
                return tokenBundle.id_token;
            }
*/            else if (tokenType == 4){
                //Console.WriteLine(string.Concat("Access Token: ", tokenBundle.refresh_token));
                return tokenBundle.refresh_token;
            }
            return responseString;
        }

        public async Task Authenticate() {
            lastLogin = DateTime.Now;
            var client = new HttpClient();

            // Create the HttpContent for the form to be posted.
            var requestContent = new FormUrlEncodedContent(new[] {
            new KeyValuePair<string, string>("grant_type", this.grantType),
            new KeyValuePair<string, string>("username", this.username),
            new KeyValuePair<string, string>("password", this.password),
            new KeyValuePair<string, string>("client_id", this.clientId),
            //new KeyValuePair<string, string>("scope", this.scope),
            new KeyValuePair<string, string>("client_secret", this.clientSecret),
            });

            // Get the response.
            HttpResponseMessage response = await client.PostAsync(
                this.identityProviderGetToken,
                requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            tokenBundleString = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TokenBundle));
                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(this.tokenBundleString));
                stream.Position = 0;
                tokenBundle = (TokenBundle)jsonSerializer.ReadObject(stream);
            }
            else {
                Console.WriteLine("Status Code: " + response.StatusCode.ToString());
            }
        }
        

        public async Task RefreshToken(String token) {
            lastLogin = DateTime.Now;
            var client = new HttpClient();

            // Create the HttpContent for the form to be posted.
            var requestContent = new FormUrlEncodedContent(new[] {
            new KeyValuePair<string, string>("grant_type", "refresh_token"),
            new KeyValuePair<string, string>("client_id", this.clientId),
            new KeyValuePair<string, string>("client_secret", this.clientSecret),
            new KeyValuePair<string, string>("refresh_token", token),
            new KeyValuePair<string, string>("scope", "openid"),
            });

            // Get the response.
            HttpResponseMessage response = await client.PostAsync(
                this.identityProviderGetToken,
                requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            tokenBundleString = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(TokenBundle));
                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(this.tokenBundleString));
                stream.Position = 0;
                tokenBundle = (TokenBundle)jsonSerializer.ReadObject(stream);
            }
            else
            {
                Console.WriteLine("Status Code: " + response.StatusCode.ToString());
            }
        }

        public int TimeUntilTokenValid() {
            int timeTokenValid=0;
            int timeSinceLogin = int.Parse((DateTime.Now - lastLogin).ToString());
            return timeTokenValid - timeSinceLogin;
        }

    }
}
