﻿
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace s3i_cs_lib
{
    public class S3IMessage
    {
        private String pgpMsg;
        private String msg;

        private String inputFile;
        private String outputFile;

    }

    [DataContract]
    public class UMsg
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyToEndpoint { get; set; }
        [DataMember]
        public List<Attachment> attachments { get; set; }
        [DataMember]
        public String subject { get; set; }
        [DataMember]
        public String text { get; set; }
    }
    
    public class SerMsg
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyToEndpoint { get; set; }
        [DataMember]
        public String serviceType { get; set; }
        [DataMember]
        public List<Parameter> parameters { get; set; }       
    }

    public class Parameter
    {
        [DataMember]
        public String key { get; set; }
        [DataMember]
        public String value { get; set; }
    }

    public class RepMsg
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String serviceType { get; set; }
        [DataMember]
        public String replyingToMessage { get; set; }
        [DataMember]
        public List<Parameter> results { get; set; }
    }

    public class GVRq
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyToEndpoint { get; set; }
        [DataMember]
        public String attributePath { get; set; }
    }

    public class GVRp
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyingToMessage { get; set; }
        [DataMember]
        public List<Parameter> value { get; set; }
    }

    public class ESRq
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyToEndpoint { get; set; }
        [DataMember]
        public String RQL { get; set; }
    }

    public class ESRp
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyingToMessage { get; set; }
        [DataMember]
        public String subscriptionId { get; set; }
        [DataMember]
        public Boolean ok { get; set; }
    }
    
    public class EURq
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyToEndpoint { get; set; }
        [DataMember]
        public String subscriptionId { get; set; }
    }

    public class EURp
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String replyingToMessage { get; set; }
        [DataMember]
        public String subscriptionId { get; set; }
        [DataMember]
        public Boolean ok { get; set; }
    }

    public class EvMsg
    {
        [DataMember]
        public String sender { get; set; }
        [DataMember]
        public String identifier { get; set; }
        [DataMember]
        public List<String> receivers { get; set; }
        [DataMember]
        public String messageType { get; set; }
        [DataMember]
        public String subscriptionId { get; set; }
        [DataMember]
        public Position position { get; set; }
        [DataMember]
        public DateTime time { get; set; }
        [DataMember]
        public String content { get; set; }
    }

    public class Position
    {
        [DataMember]
        public String longitude { get; set; }
        [DataMember]
        public String latitude { get; set; }
    }



    public class UserMessage : S3IMessage
    {
        public String sender;
        public String identifier;
        public String messageType;
        public String replyToEndpoint;
        public List<Attachment> attachments = new List<Attachment>();
        public List<String> receivers = new List<String>();
        public String subject;
        public String text;
        public String rawBody;
        public String encBody;

        private String pgpMsg;
        public UMsg msg = new UMsg();

        private String inputFile;
        private String outputFile;
        public void FillUserMessage(String senderUUID, List<String> receiverUUIDs, String senderEndpoint, String subject, String text, String msgUUID)
        {
            msg.sender = senderUUID;
            msg.identifier = msgUUID;
            msg.replyToEndpoint = senderEndpoint;
            msg.subject = subject;
            msg.text = text;
            msg.attachments = new List<Attachment>();
            msg.receivers = receiverUUIDs;
            msg.messageType = "userMessage";
        }

        public String getMsg() {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(UMsg));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, msg);
            return Encoding.ASCII.GetString(stream.ToArray());
        }

        public void setMsg(String input)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(UMsg));
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);
            msg = (UMsg)ser.ReadObject(stream);
        }

        public String getRawMsg()
        {
            return rawBody;
        }
        public String getEncMsg()
        {
            return encBody;
        }

        public String ConvertPgpToMsg()
        {
            return "";
        }
    }

    public class ServiceRequest
    {
        private String pgpMsg;
        public SerMsg msg = new SerMsg();

        public void FillServiceRequest(String senderUUID, List<String> receiverUUIDs, String senderEndpoint, String serviceType, List<Parameter> parameters, String msgUUID)
        {
            msg.sender = senderUUID;
            msg.identifier = msgUUID;
            msg.replyToEndpoint = senderEndpoint;
            msg.receivers = receiverUUIDs;
            msg.messageType = "serviceRequest";
            msg.serviceType = serviceType;
            msg.parameters = parameters;
        }

        public String getMsg()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SerMsg));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, msg);
            return Encoding.ASCII.GetString(stream.ToArray());
        }

        public void setMsg(String input)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(SerMsg));
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);
            msg = (SerMsg)ser.ReadObject(stream);
        }

        public void ConvertPgpToMsg()
        {

        }
    }

    class ServiceReply
    {

        private String pgpMsg;
        public RepMsg msg = new RepMsg();

        public void FillServiceReply(String senderUUID, String receiverUUID, String serviceType, List<Parameter> results, String msgUUID, String replyingToUUID)
        {
            msg.sender = senderUUID;
            msg.identifier = msgUUID;
            msg.replyingToMessage = replyingToUUID;
            msg.receivers = new List<String> { receiverUUID };
            msg.serviceType = serviceType;
            msg.results = results;
        }

        public String getMsg()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RepMsg));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, msg);
            return Encoding.ASCII.GetString(stream.ToArray());
        }

        public void setMsg(String input)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RepMsg));
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);
            msg = (RepMsg)ser.ReadObject(stream);
        }

        public void ConvertPgpToMsg()
        {

        }
    }

    class GetValueRequest
    {

        private String pgpMsg;
        public GVRq msg = new GVRq();

        public void FillGetValueRequest(String senderUUID, String receiverUUID, String senderEndpoint, String attributePath, String msgUUID)
        {
            msg.sender = senderUUID;
            msg.identifier = msgUUID;
            msg.receivers = new List<String> { receiverUUID };
            msg.replyToEndpoint = senderEndpoint;
            msg.attributePath = attributePath;
        }

        public String getMsg()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(GVRq));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, msg);
            return Encoding.ASCII.GetString(stream.ToArray());
        }

        public void setMsg(String input)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(GVRq));
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);
            msg = (GVRq)ser.ReadObject(stream);
        }

        public void ConvertPgpToMsg()
        {

        }
    }

    class GetValueReply
    {

        private String pgpMsg;
        public GVRp msg = new GVRp();

        public void FillGetValueReply(String senderUUID, String receiverUUID, List<Parameter> result, String msgUUID, String replyingToUUID)
        {
            msg.sender = senderUUID;
            msg.identifier = msgUUID;
            msg.receivers = new List<String> { receiverUUID };
            msg.replyingToMessage = replyingToUUID;
            msg.value = result;
        }

        public String getMsg()
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(GVRp));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, msg);
            return Encoding.ASCII.GetString(stream.ToArray());
        }

        public void setMsg(String input)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(GVRp));
            byte[] byteArray = Encoding.UTF8.GetBytes(input);
            MemoryStream stream = new MemoryStream(byteArray);
            msg = (GVRp)ser.ReadObject(stream);
        }

        public void ConvertPgpToMsg()
        {

        }
    }
}
