﻿using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace s3i_cs_lib
{
    public class Attachment
    {
        public string Boundary { get; set; }
        public string ContentId { get; set; }
        public string MediaType { get; set; }
        public string Name { get; set; }
        public Encoding NameEncoding { get; set; }
        public Stream ContentStream { get; set; }

        public Attachment()
        {
            ContentStream = new MemoryStream();
        }

        public Attachment(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);
            ContentStream = new MemoryStream(File.ReadAllBytes(fileName));
            ContentStream.Seek(0, SeekOrigin.Begin);
            Name = fi.Name;
        }

        public Attachment(string fileName, string mediaType)
        {
            FileInfo fi = new FileInfo(fileName);
            ContentStream = new MemoryStream(File.ReadAllBytes(fileName));
            ContentStream.Seek(0, SeekOrigin.Begin);
            Name = fi.Name;
            MediaType = mediaType;
        }

        public Attachment(string fileName, ContentType contentType)
        {
            FileInfo fi = new FileInfo(fileName);
            ContentStream = new MemoryStream(File.ReadAllBytes(fileName));
            ContentStream.Seek(0, SeekOrigin.Begin);
            Name = fi.Name;

            MediaType = contentType.MediaType;
            Boundary = contentType.Boundary;
        }

        public Attachment(Stream contentStream)
            : this()
        {
            contentStream.CopyTo(ContentStream);
            ContentStream.Seek(0, SeekOrigin.Begin);
        }

        public Attachment(Stream contentStream, string name)
            : this()
        {
            Name = name;
            contentStream.CopyTo(ContentStream);
            ContentStream.Seek(0, SeekOrigin.Begin);
        }

        public Attachment(Stream contentStream, string name, string mediaType)
            : this()
        {
            Name = name;
            contentStream.CopyTo(ContentStream);
            ContentStream.Seek(0, SeekOrigin.Begin);
            MediaType = mediaType;
        }

        public Attachment(Stream contentStream, ContentType contentType)
            : this()
        {
            contentStream.CopyTo(ContentStream);
            ContentStream.Seek(0, SeekOrigin.Begin);
            Name = contentType.Name;
            MediaType = contentType.MediaType;
            Boundary = contentType.Boundary;
        }

        /// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Mail.AlternateView" /> and optionally releases the managed resources. </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources. </param>
        protected void Dispose(bool disposing)
        {
            if (ContentStream != null)
                ContentStream.Dispose();
        }
    }

    public class AttachmentCollection : Collection<Attachment>
    {
    }

    public struct ContentType
    {
        /// <summary>Gets or sets the value of the boundary parameter included in the Content-Type header represented by this instance.</summary>
        public string Boundary;
        /// <summary>Gets or sets the value of the charset parameter included in the Content-Type header represented by this instance.</summary>
        public string CharSet;
        /// <summary>Gets or sets the media type value included in the Content-Type header represented by this instance.</summary>
        public string MediaType;
        /// <summary>Gets or sets the value of the name parameter included in the Content-Type header represented by this instance.</summary>
        public string Name;
    }
}
