﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace s3i_cs_lib
{
    [DataContract]
    class Person 
    {
        [DataMember]
        private String username;
        [DataMember]
        private String password;

        public Person(String user, String pw)
        {
            username = user;
            password = pw;
        }
    }

    [DataContract]
    class Encrypted
    {
        [DataMember]
        private Boolean encrypted;

        public Encrypted(Boolean encrypted)
        {
            this.encrypted = encrypted;
        }
    }

    [DataContract]
    public class CreateThingResponse
    {
        [DataMember]
        public string identifier;

        [DataMember]
        public string secret;
    }

    public class Config
    {
        private String serverURL;
        private String token;

        public Config(String serverURL, String token)
        {
            this.serverURL = serverURL;
            this.token = token;
        }

        public async Task<string> CreatePerson(String username, String password)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            Person user = new Person(username, password);
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Person));
            MemoryStream stream = new MemoryStream();
            // Serializer the User object to the stream.
            jsonSerializer.WriteObject(stream, user);
            byte[] json = stream.ToArray();
            stream.Close();

            var requestContent = new StringContent(Encoding.UTF8.GetString(json, 0, json.Length), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(this.serverURL + "persons/", requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeletePerson(String username)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await client.DeleteAsync(this.serverURL + "persons/" + username);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> CreateThing(Boolean encrypted)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            Encrypted enc = new Encrypted(encrypted);
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Encrypted));
            MemoryStream stream = new MemoryStream();
            // Serializer the User object to the stream.
            jsonSerializer.WriteObject(stream, enc);
            byte[] json = stream.ToArray();
            stream.Close();

            var requestContent = new StringContent(Encoding.UTF8.GetString(json, 0, json.Length), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(this.serverURL + "things/", requestContent);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeleteThing(String thingID)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await client.DeleteAsync(this.serverURL + "things/" + thingID);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> CreateCloudCopy(String thingID)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await client.PostAsync(this.serverURL + "things/" + thingID + "/repository", null);

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }

        public async Task<string> DeleteCloudCopy(String thingID)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            HttpResponseMessage response = await client.DeleteAsync(this.serverURL + "things/" + thingID + "/repository");

            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            responseBodyAsText = responseBodyAsText.Replace("<br>", Environment.NewLine); // Insert new lines

            if (response.IsSuccessStatusCode)
            {
                return responseBodyAsText;
            }
            else
            {
                return string.Concat("Status Code: " + response.StatusCode.ToString());
            }
        }
    }
}