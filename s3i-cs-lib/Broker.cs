﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing.Impl;

namespace s3i_cs_lib
{
    public class Broker
    {
        private String authForm;
        private String username;
        private String password;
        private String contentType;
        private String x509path;
        private String host;

        public Broker(String authForm, String username, String password, String contentType, String x509path, String host) {
            this.authForm = authForm;
            this.username = username;
            this.password = password;
            this.contentType = contentType;
            this.x509path = x509path;
            this.host = host;
        }

        public ConnectionFactory BuildConnectionParameters()
        {
            ConnectionFactory factory = new ConnectionFactory() { HostName = host };

            if (authForm.Equals("Username/Password"))
            {
                factory.UserName = username;
                factory.Password = password;
                //factory.Protocol = Protocols.FromEnvironment();
                factory.Port = AmqpTcpEndpoint.UseDefaultPort;
            }
            else if (authForm.Equals("X509"))
            {
                factory.Ssl.ServerName = x509path + "client_key.pem";
                // Path to my .p12 file.
                factory.Ssl.CertPath = x509path + "client_certificate.pem";
                // Passphrase for the certificate file - set through OpenSSL
                factory.Ssl.CertPassphrase = x509path + "client_key.pem";
                factory.Port = 5671;
            }
            factory.HostName = host;
            factory.VirtualHost = "s3i";
            return factory;
        }
        
        public IConnection BuildConnection(ConnectionFactory factory)
        {
            IConnection connection = factory.CreateConnection();
            return connection;
        }

        public IModel BuildChannel(IConnection connection)
        {
            IModel channel = connection.CreateModel();
            return channel;
        }

        public void Receive(String queue, Boolean cb, Action<string> callback)
        {
            ConnectionFactory factory = BuildConnectionParameters();
            IConnection connection = BuildConnection(factory);
            IModel channel = BuildChannel(connection);
            channel.QueueDeclare(queue: queue,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                string message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                callback(message);
                //channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };

            String conTag = channel.BasicConsume(queue, cb, consumer);
        }

        public void ReceiveOnce(String queue, Boolean cb, Action<string> callback)
        {
            ConnectionFactory factory = BuildConnectionParameters();
            IConnection connection = BuildConnection(factory);
            IModel channel = BuildChannel(connection);
            channel.QueueDeclare(queue: "task_queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, ea) =>
            {
                var body = ea.Body.ToArray();
                string message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                callback(message);
            };
            channel.BasicGet(queue, cb);
        }

        public void Send(List<string> receiverEndpoints, byte[] message)
        {
            ConnectionFactory factory = BuildConnectionParameters();
            IConnection connection = BuildConnection(factory);
            IModel channel = BuildChannel(connection);
            IBasicProperties props = channel.CreateBasicProperties();
            props.ContentType = contentType;
            props.DeliveryMode = 2;
            try
            {
                foreach (String rE in receiverEndpoints){
                    channel.BasicPublish("demo.direct", rE, true, props, message);
                }
            }
            catch (Exception e)
            {
                if (e.Source != null)
                    Console.WriteLine("Exception source: {0}", e.Source);
            }

        }
    }
}
