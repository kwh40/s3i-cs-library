﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using s3i_cs_lib;

namespace s3i_cs_lib
{
    namespace Directory
    {
        /*
        Nur die s3i Klasse darf Thing erstellen.

        Noch fehlend (aus api.h kopiert):

            private:
            // Things are only created by the s3i class
            friend class s3i;
            Thing(s3i* s3i);

            list<ThingId> childThings;
           

        */
        public class Thing//: Entry
        {
            private string datamodel;
            private string identifier;
            private string name;
            Miscellaneous.Location location;
            private byte publicKey; 
            ThingType type;
            private JObject thing;
            private List<Endpoint> allEndpoints;
            private List<Endpoint> defaultEndpoints;

            public Thing(string thing)
                // Test: pending
            {
                this.thing = JsonConvert.DeserializeObject<JObject>(thing);
                datamodel = this.thing.SelectToken("attributes.dataModel").ToString();

                /*
                 * System.NullReferenceException wenn versucht wird Variable zu initieren aus NULL.
                 * NULL wenn Variable nicht im JSON des Directory-Eintrags vorhanden ist. 
                */
                if (this.thing.SelectToken("attributes.name", false) != null)
                {
                    name = this.thing.SelectToken("attributes.name", false).ToString();
                }

                if (this.thing.SelectToken("attributes.location.longitude") != null)
                {
                    location.Longitude = (double) this.thing.SelectToken("attributes.location.longitude");
                }

                if (this.thing.SelectToken("attributes.location.lotitude") != null)
                {
                    location.Latitude = (double)this.thing.SelectToken("attributes.location.lotitude");
                }

                if (this.thing.SelectToken("attributes.publicKey") != null)
                {
                    publicKey = (byte)this.thing.SelectToken("attributes.publicKey");
                }

                allEndpoints = new List<Endpoint>();
                foreach (JToken endpoint in this.thing.SelectToken("attributes.allEndpoints"))
                {
                    allEndpoints.Add(new Endpoint(endpoint.ToString()));
                }

                switch (this.thing.SelectToken("attributes.type").ToString())
                {
                    case "component":
                        type = ThingType.component;
                        break;
                    case "service":
                        type = ThingType.service;
                        break;
                    case "hmi":
                        type = ThingType.hmi;
                        break;
                }
            }

            public async Task<Policy.PolicyEntry> GetMyPolicy(string token)
            // Test: passed
            {
                string policyId = thing.SelectToken("policyId").ToString();
                Policy.PolicyEntryFactory factory = new Policy.PolicyEntryFactory();
                return await factory.Pull(token, "https://dir.s3i.vswf.dev/api/2/", policyId);
            }

            public List<Endpoint> GetDefaultEndpoints()
            // Test: passed
            /* 
            default Endpoints beginnen mit "https://ditto..."
             */
            {
                defaultEndpoints = new List<Endpoint>();

                foreach (Endpoint endpoint in allEndpoints)
                {
                    if (endpoint.Uri.Length > 13 && endpoint.Uri.Substring(0, 13) == "https://ditto")
                    {
                        defaultEndpoints.Add(new Endpoint(endpoint.Uri));
                    }
                }

                return defaultEndpoints;
            }

            public List<Endpoint> GetAllEndpoints()
            // Test: passed
            {
                return allEndpoints;
            }

            public List<Role> GetRoles()
            // Aus api.h: Rollen noch nicht im s3i Standpunkt definiert
            {
                return null;
            }

            public List<Thing> GetChildren()
            // Test: pending
            // Erkennung: class: ml40:Thing 
            {
                IEnumerable<JToken> thingChildren = this.thing.SelectTokens("$.attributes.thingStructure.links[?(@.association == 'features')].target.links[?(@.class == 'ml40::Thing')]");
                
                foreach (JToken child in thingChildren)
                {
                    /* to do (Theoretisch): mit Identifier aus Directory abrufen und Thing erstellen. 
                     * Problem: Bsp. Harvester (s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63) hat für Komponenten eigene s3i IDs
                     *          nicht aber das hinterlegte Thing in der Directory.
                     * Frage: Child mit ID abrufen oder Konstruktor schreiben welcher übergebenen JSON String in Thing umwandelt? 
                     */
                }

                return null;
            }

            public Thing GetDefaultHMI()
            // Wo genau zu finden? 
            {
                return null;
            }

            public Object GetThingStructure()
            // Test: passed
            {
                Object thingStructureObject = new Object(this.thing);
                //Object thingStructureObject = new Object(this.thing.SelectToken("attributes.thingStructure"));
                return thingStructureObject;
            }

            public void AddEndpoint(string endpoint)
            // Test: passed
            {
                if (allEndpoints.Contains(new Endpoint(endpoint)) == false)
                {
                    allEndpoints.Add(new Endpoint(endpoint));
                }
            }

            public Boolean RemoveEndpoint(string endpoint)
            // Test: passed
            {
                Boolean delete = false;

                foreach (Endpoint ep in allEndpoints)
                {
                    if (ep.Uri == endpoint)
                    {
                        delete = allEndpoints.Remove(ep);
                        break;
                    }
                }
                return delete;
            }

            public void SetName(string name)
            {

            }
        }

        [Flags]
        internal enum ThingType
        {
            component,
            service,
            hmi
        }

        public class Endpoint
        {
            string uri;

            internal Endpoint(string uri)
            {
                this.uri = uri;
            }

            // Aufruf über Endpoint.Uri
            public string Uri { get => uri; }
        }

        public class Value
        {
            JObject s3iValue;
            string attribute;
            List<string> value;

            public Value(JObject s3iValue)
            {
                this.s3iValue = s3iValue;
                attribute = s3iValue.SelectToken("attribute").ToString();

                value = new List<string>();
                if (s3iValue.SelectToken("value").Count() > 1)
                {
                    foreach (JToken value in s3iValue.SelectToken("value"))
                    {
                        this.value.Add(value.ToString());
                    }
                }
                else
                {
                    this.value.Add(s3iValue.SelectToken("value").ToString());
                }


            }

            private List<Endpoint> GetEndpoints()
            // keine Beispiele für Implementation gefunden
            {
                return null;
            }
        }


        public class Service
            // keine Beispiele für Implementation gefunden
        {
            string serviceType;
            //List<KeyValuePair<string, type>> parameterTypes;
            //List<KeyValuePair<string, type>> resulTypes;

            public Service()
            {

            }

            public List<Endpoint> GetEndpoints()
            {
                return null;
            }

            // map<string, type> execute(const map<string, variant>& parameters, const Endpoint& endpoint);

        }


        public class Object
        {

            private JObject s3iThingStructure;
            private JObject s3iThing;
            private string identifier;
            private string objectClass;
            private List<Link> links;
            private List<Value> values;

            public Object(JObject s3iThing)
            {
                /*
                Fallunterscheidung ob:
                               
                    1. gesamter Directory-Eintrag
                        oder
                    2. Abschnitt thingStructure 

                übergeben wurde.
                */
                if (s3iThing.SelectToken("attributes.thingStructure") != null)
                {
                    this.s3iThingStructure = s3iThing.SelectToken("attributes.thingStructure").ToObject<JObject>();
                    objectClass = s3iThingStructure.SelectToken("class").ToString();

                }

                else
                {
                    this.s3iThingStructure = s3iThing;
                    if (s3iThing.SelectToken("identifier") != null)
                    {
                        identifier = s3iThing.SelectToken("identifier").ToString();
                    }

                    if (s3iThingStructure.SelectToken("class") != null)
                    {
                        objectClass = s3iThingStructure.SelectToken("class").ToString();
                    }
                }
            }

            public string GetIdentifier()
                // Test:passed
            {
                return identifier;
            }

            public string GetClass()
                // Test:passed
            {
                return objectClass;
            }

            public List<Link> GetLinks()
                // Test:passed
            {
                links = new List<Link>();
                foreach (JObject link in s3iThingStructure.SelectToken("links"))
                {
                    links.Add(new Link(link.ToObject<JObject>()));
                }

                return links;
            }

            public List<Value> GetValues()
                // Test: passed

                // benötigt ein Object erstellt aus einem Link-Target. 
                // !!NICHT!! komplette ThingStructure als Object
            {
                values = new List<Value>();
                foreach (JToken value in s3iThingStructure.SelectToken("values"))
                {
                    values.Add(new Value(value.ToObject<JObject>()));
                }

                return values;
            }

            public List<Service> GetServices()
                // keine Beispiele für Implementation gefunden
            {
                return null;
            }
        }

        public class Link
        {
            private JObject s3iLink;
            private string association;
            private Object target;

            public Link(JObject s3iLink)
                // Test: passed
            {
                this.s3iLink = s3iLink;
                association = s3iLink.SelectToken("association").ToString();
                target = new Object(s3iLink.SelectToken("target").ToObject<JObject>());
            }

            public string GetAssociation()
                // Test: passed
            {
                return association;
            }

            public Object GetTarget()
                // Test: passed
            {
                return target;
            }
        }

        public class Role
        {
            // Aus api.h: Rollen noch nicht im s3i Standpunkt definiert
        }
    }
}
