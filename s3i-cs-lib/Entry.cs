﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s3i_cs_lib
{
    public interface Entry
    {
        //dynamic Pull(string policyID, string identityToken, string url);
        Task<dynamic> Pull(string id, string identityToken, string url);
        //dynamic Commit();
        Task<dynamic> Commit(string identityToken, string url, string policyID, string newPolicy);
        //dynamic Delete();
    }
}
