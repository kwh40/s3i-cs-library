﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using s3i_cs_lib;

namespace s3i_cs_lib
{
    namespace Policy
    {
        public class PolicyEntryFactory : Entry
        {
            public async Task<dynamic> Pull(string identityToken, string url, string policyID)
            {
                DittoManager dittoManager = new DittoManager(url, identityToken);
                string response = await dittoManager.QueryPolicyIDBasedAsync(policyID);

                if (response.Contains("Status Code: "))
                {
                    return response;
                }
                else
                {
                    return new PolicyEntry(response);
                }
            }

            // Neues Subjects hinzufügen liefert: "Forbidden"
            public async Task<dynamic> Commit(string identityToken, string url, string policyID, string newPolicy)
            {
                DittoManager dittoManager = new DittoManager(url, identityToken);
                string response = await dittoManager.UpdatePolicyIDBasedAsync(policyID, newPolicy);

                return response;
            }
        }

        internal class Policy
        {
            [JsonProperty]
            internal string policyId { get; set; }

            [JsonProperty]
            internal Dictionary<string, GroupJson> entries { get; set; }
            
        } 

        internal class GroupJson
        {
            [JsonProperty]
            internal Dictionary<string, SubjectType> subjects { get; set; }

            [JsonProperty]
            internal Dictionary<string, ResourceAttribute> resources { get; set; }
        }

        internal class SubjectType
        {
            [JsonProperty]
            internal string type { get; set; }
        }

        internal class ResourceAttribute
        {
            [JsonProperty]
            internal List<string> grant { get; set; }
            
            [JsonProperty]
            internal List<string> revoke { get; set; }
        }

        public class PolicyEntry
        {
            private JObject policy;
            private string id;
            // directory oder repository url
            private string url;
            private dynamic policyGroups = new List<KeyValuePair<string, Group>>();
            private string eTag;

            internal PolicyEntry()
            {
            }
            internal PolicyEntry(string policy)
            {
                this.policy = JsonConvert.DeserializeObject<JObject>(policy);
                this.id = ((this.policy).SelectToken("policyId")).ToString();

                foreach (JToken group in (this.policy).SelectToken("entries"))
                {
                    string name = group.ToObject<JProperty>().Name;
                    policyGroups.Add(new KeyValuePair<string, Group>(name, new Group(name, group.First.ToString())));
                }
            }

            public string PolicyToString()
            {
                Policy policy = new Policy();
                policy.policyId = this.id;
                policy.entries = new Dictionary<string, GroupJson>();

                foreach (KeyValuePair<string, Group> group in policyGroups)
                {
                    GroupJson groupJson = new GroupJson();
                    groupJson.subjects = new Dictionary<string, SubjectType>();
                    groupJson.resources = new Dictionary<string, ResourceAttribute>();

                    foreach (KeyValuePair<string, Subject> subject in group.Value.GetGroupSubjects())
                    {
                        groupJson.subjects.Add(subject.Key, new SubjectType { type = subject.Value.GetType() });
                    }

                    foreach (KeyValuePair<string, Resource> resource in group.Value.GetGroupResources())
                    {
                        // Split von "READ, WRITE" zu "READ", "WRITE"
                        string grantString = resource.Value.GetGrant().ToString();
                        string[] grantArray = grantString.Split(',');
                        List <string> grantList = new List<string>();

                        for (int j = 0; j < grantArray.Length; j++)
                        {
                            string s = grantArray[j];
                            for (int i = 0; i < s.Length; i++)
                            {
                                if (s[i] == ' ')
                                {
                                    s = s.Remove(i, i+1);
                                }
                                else if (s[i] == '0')
                                {
                                    break;
                                }
                            }
                            grantList.Add(s);
                        }

                        // Split von "READ, WRITE" zu "READ", "WRITE"
                        string revokeString = resource.Value.GetRevoke().ToString();
                        string[] revokeArray = revokeString.Split(',');
                        List<string> revokeList = new List<string>();

                        for (int j = 0; j < revokeArray.Length; j++)
                        {
                            string s = revokeArray[j];
                            for (int i = 0; i < s.Length; i++)
                            {
                                if (s[i] == ' ')
                                {
                                    s = s.Remove(i, i + 1);
                                }
                                else if (s[i] == '0')
                                {
                                    break;
                                }
                            }
                        }

                        groupJson.resources.Add(resource.Key, new ResourceAttribute { grant = grantList, revoke = revokeList });
                    }

                    policy.entries.Add(group.Key, groupJson);
                }
                return JsonConvert.SerializeObject(policy);
            }

            public string GetId()
            {
                // passed
                return id;
            }

            public string GetETag()
            {
                // etag vom letzten Cloudupdate
                // Referenz: https://www.eclipse.org/ditto/httpapi-concepts.html#etag
                return eTag;
            }

            public List<KeyValuePair<string, Group>> GetPolicyGroups() 
            {
                // passed
                return policyGroups;
            }

            // name: "owner" oder "observer"
            public Group GetGroup(string name)
            // passed
            {
                foreach (KeyValuePair<string, Group> pair in policyGroups)
                {
                    if (pair.Key == name){
                        return pair.Value;
                    }
                }
                return null;
            }

            public Group AddGroup(string name)
            {
                // Gruppe erstellen nur mit Name sinnvoll? 
                //  L Inhalt müsste über manuelle Methodenaufrufe erstellt werden

                // passed
                if (policyGroups.Contains(new KeyValuePair<string, Group>(name, GetGroup(name))) == false)
                {
                    Group newGroup = new Group(name);
                    policyGroups.Add(new KeyValuePair<string, Group>(name, newGroup));
                    return newGroup;
                }
                else
                {
                    return null;
                }
            }

            public bool DeleteGroup(string name)
            {
                // passed
                return policyGroups.Remove(new KeyValuePair<string, Group>(name, GetGroup(name)));
            }

            // Spezielle Methoden für Rolle "observer" hinzufügen. Können sich jedoch noch laufend ändern.
        }
        public class Group
        {
            // Name der Policygruppe
            private string name;
            private JObject group;
            // key: Subject::id
            private dynamic groupSubjects = new List<KeyValuePair<string, Subject>>();
            // key: Resource::path
            private dynamic groupResources = new List<KeyValuePair<string, Resource>>();

            internal Group(string name, string group)
            {
                this.name = name;
                this.group = JsonConvert.DeserializeObject<JObject>(group);

                foreach (JToken subject in (this.group).SelectToken("subjects"))
                {
                    string subjectName = subject.ToObject<JProperty>().Name;
                    groupSubjects.Add(new KeyValuePair<string, Subject>(subjectName, new Subject(subjectName, subject.First.Value<string>("type"))));
                }
                foreach (JToken resource in (this.group).SelectToken("resources"))
                {
                    string resourceName = resource.ToObject<JProperty>().Name;
                    groupResources.Add(new KeyValuePair<string, Resource>(resourceName, new Resource(resource)));
                }
            }

            internal Group(string name, List<Subject> groupSubjects, List<Resource> groupResource)
            {
                // Wird z.Z. nirgends verwendet
                this.name = name;
                this.groupSubjects = groupSubjects;
                this.groupResources = groupResource;
            }

            internal Group(string name)
            {
                // Lediglich bei AddGroup in Klasse PolicyEntry verwendet
                this.name = name;
            }

            public List<KeyValuePair<string, Subject>> GetGroupSubjects()
            {
                // passed
                return groupSubjects;
            }

            public List<KeyValuePair<string, Resource>> GetGroupResources()
            {
                // passed
                return groupResources;
            }

            public Subject GetSubject(string id)
            {
                // passed
                foreach (KeyValuePair<string, Subject> pair in groupSubjects)
                {
                    if (pair.Value.GetId() == id)
                    {
                        return pair.Value;
                    }
                }
                return null;
            }

            public Resource GetResource(string path)
            {
                // passed
                foreach (KeyValuePair<string, Resource> pair in groupResources)
                {
                    if (pair.Value.GetPath() == path)
                    {
                        return pair.Value;
                    }
                }
                return null;
            }

            public Subject AddSubject(string id, string type)
            {
                // passed
                Subject newSubject = new Subject(id, type);
                groupSubjects.Add(new KeyValuePair<string, Subject>(id, newSubject));
                return newSubject;
            }

            public Resource AddResource(string path, Resource.PermissionType grant = 0, Resource.PermissionType revoke = 0)
            {
                // passed
                Resource newResource = new Resource(path, grant, revoke);
                groupResources.Add(new KeyValuePair<string, Resource>(path, newResource));
                return newResource;
            }

            public bool DeleteSubject(string id)
            {
                // passed
                return groupSubjects.Remove(new KeyValuePair<string, Subject>(id, GetSubject(id)));
            }

            public bool DeleteResource(string path)
            {
                // passed
                return groupResources.Remove(new KeyValuePair<string, Resource>(path, GetResource(path)));
            }
        }

        public class Subject
        {
            // Format: "nginx:<ID>"
            private string id;

            // speichert Validierungsdatum (optional)
            // externe Bib (TimeStamp), damit nach ISO-8601 Sring konvertieren
            //private TimeStamp expiringTimestamp;
            // subject Beschreibung (optional)
            private string type;

            internal Subject(string id, string type)
            {
                this.id = id;
                this.type = type;
            }

            // TimeStamp: externe Bibliothek
            //public Subject(string id, TimeStamp expiringTimestamp, string type)
            //{

            //}

            public string GetId()
            {
                return id;
            }

            //public TimeStamp getExpiringTimestamp()
            //{
            //    return null;
            //}

            public string GetType()
            {
                return type;
            }

            //public void setExpiringTimestamp(Timestamp expiringTimestamp)
            //{

            //}

            public void SetType(string type)
            {
                // passed
                this.type = type;
            }
        }

        public class Resource
        {
            [Flags]
            public enum PermissionType
            {
                Read = 1 << 0, // 1
                Write = 1 << 1, // 2
                Execute = 1 << 2 // 4
            }

            // path to the resource (e.g.: "thing:/", "policy:/", "thing:/features/featureX/properties/location")
            private JToken data;
            private string path;
            private PermissionType grant;
            private PermissionType revoke;

            internal Resource(JToken data)
            {
                this.data = data;
                this.path = data.ToObject<JProperty>().Name;

                foreach (JToken pathPermissions in this.data)
                {
                    foreach (JToken permission in pathPermissions)
                    {
                        string name = permission.ToObject<JProperty>().Name; // grant; revoke

                        foreach (JToken permissionType in permission.First)
                        {
                            if (permissionType.ToString() == "READ")
                            {
                                if (name == "grant")
                                {
                                    grant = PermissionType.Read;
                                    continue;
                                }
                                else if (name == "revoke")
                                {
                                    revoke = PermissionType.Read;
                                    continue;
                                }
                            }

                            if (permissionType.ToString() == "WRITE")
                            {
                                if (name == "grant")
                                {
                                    grant |= PermissionType.Write;
                                    continue;
                                }
                                else if (name == "revoke")
                                {
                                    revoke |= PermissionType.Write;
                                    continue;
                                }
                            }

                            if (permissionType.ToString() == "EXECUTE")
                            {
                                if (name == "grant")
                                {
                                    grant |= PermissionType.Execute;
                                    continue;
                                }
                                else if (name == "revoke")
                                {
                                    revoke |= PermissionType.Execute;
                                    continue;
                                }
                            }
                        }
                    }
                }
            }

            internal Resource(string path, PermissionType grant, PermissionType revoke)
            {
                this.path = path;
                this.grant = grant;
                this.revoke = revoke;
            }

            public string GetPath()
            {
                // passed
                return path;
            }

            public PermissionType GetGrant()
            {
                // passed
                return grant;
            }

            public PermissionType GetRevoke()
            {
                // passed
                return revoke;
            }

            public void SetGrant(PermissionType grant)
            {
                // passed
                this.grant = grant;
            }

            public void SetRevoke(PermissionType revoke)
            {
                // passed
                this.revoke = revoke;
            }
        }
    }
}
